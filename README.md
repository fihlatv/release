JavaScript EJS is the latest version of Easy Java/JavaScript Simulations that creates HTML+JavaScript simulations.

Download the latest release and install it by unzipping in any suitable directory of your computer.

JavaScript EJS is compatible with Java 9 up to Java 13.